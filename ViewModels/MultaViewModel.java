package ViewModels;

import Models.Multa;
import Views.Application;
import java.time.LocalDateTime;

/**
 * Modelo de vista de las multas
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class MultaViewModel extends BaseViewModel<Multa>
{    
    public MultaViewModel(Application app)
    {
        super(app);
    }
    
    /**
     * Cancela una multa
     */
    public void cancelarMulta(Multa multa)
    {
        if (multa.getFechaCancelacion() != null) {
            App.displayAlert("La multa ya ha sido cancelada");
            return;
        }
        multa.setFechaCancelacion(LocalDateTime.now());
        saveData();
        App.displayAlert("La multa se ha cancelado satisfactoriamente");
    }
    
    /**
     * Implementación del método abstracto de la superclase que devuelve una instancia IAlmacenable 
     */
    public Multa getType()
    {
        return new Multa();
    }    
}
