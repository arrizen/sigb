package ViewModels;

import Models.Libro;
import Views.Application;

/**
 * Modelo de la vista de los libros
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class LibroViewModel extends BaseViewModel<Libro>
{
    /**
     * Constructor
     */
    public LibroViewModel(Application app)
    {
        super(app);
    }
    
    /**
     * Implementación del método abstracto de la superclase que devuelve una instancia IAlmacenable 
     */
    public Libro getType()
    {
        return new Libro();
    }    
}
