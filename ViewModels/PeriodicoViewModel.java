package ViewModels;

import Models.Periodico;
import Views.Application;


/**
 * Modelo de la vista de los periódicos
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class PeriodicoViewModel extends BaseViewModel<Periodico>
{
    /**
     * Constructor
     */
    public PeriodicoViewModel(Application app)
    {
        super(app);
    }
    
    /**
     * Implementación del método abstracto de la superclase que devuelve una instancia IAlmacenable 
     */
    public Periodico getType()
    {
        return new Periodico();
    }
}
