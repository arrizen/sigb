package ViewModels;

import Models.Prestamo;
import Models.IAlmacenable;
import Models.Item;
import Models.Database;
import Models.Multa;
import Views.Application;
import java.time.LocalDateTime;
import java.time.Period;
import javax.swing.DefaultListModel;


/**
 * Modelo de la vista de los libros
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class PrestamoViewModel extends BaseViewModel<Prestamo>
{    
    /**
    * Lista para mostrar en el JList
    */
    protected DefaultListModel<Prestamo> visibledata;        
    public DefaultListModel<Prestamo> getVisibleData() {  return visibledata; }
    
    public PrestamoViewModel(Application app)
    {
        super(app);
        isprestamovm = true;
    }
    
    /**
     * Implementación del método abstracto de la superclase que devuelve una instancia IAlmacenable 
     */
    public Prestamo getType()
    {
        return new Prestamo();
    }    
    
    /**
     * Sobrecarga para dejar los préstamos del usuario activo si no es un bibliotecario
     */
    protected void loadData()
    {
        super.loadData();
        
        // Copia para mostrar
        visibledata = new DefaultListModel<Prestamo>();
        for (Object p : data.toArray()) {
            visibledata.addElement((Prestamo)p);
        }
        
        // Dejar sólo los registros del usuario actual
        if (!App.isAdmin()) {
            for (Object p : visibledata.toArray()) {
                Prestamo prestamo = (Prestamo)p;
                if (!prestamo.getUsuario().getCodigo().equals(App.getUsuario().getCodigo())) {
                    visibledata.removeElement(prestamo);
                }
            }
        }
    }
    
    /**
     * Realiza la devolución de un préstamo
     */
    public void realizarDevolucion(Prestamo prestamo)
    {
        // Sólo si el préstamo no ha sido ya devuelto
        if (prestamo.getFechaDevuelto() == null) {
            // Actualizar el registro del préstamo
            for (int i = 0; i < getData().getSize(); i++) {
                if (getData().get(i).equals(prestamo)) {
                    getData().get(i).setFechaDevuelto(LocalDateTime.now());     
                }            
            }
            saveData();
            
            // Para almacenar los elementos y las multas (si las hay)
            Database<IAlmacenable> database = new Database<IAlmacenable>();
            
            // Actualizar el elemento
            DefaultListModel<IAlmacenable> elementos = database.loadData(prestamo.getElemento());
            Item elemento = prestamo.getElemento();
            for (int i = 0; i < elementos.getSize(); i++) {
               if (((Item)elementos.get(i)).equals(elemento)) {
                    ((Item)elementos.get(i)).setDisponible(true);
                }
            }        
            database.saveData(prestamo.getElemento(), elementos); 
            
            // Si se había pasado de plazo hay que generar una multa
            if (Period.between(prestamo.getFechaDevolver().toLocalDate(), prestamo.getFechaDevuelto().toLocalDate()).getDays() > 0) {
                Multa multa = new Multa();
                multa.setPrestamo(prestamo);
                
                DefaultListModel<IAlmacenable> multas = database.loadData(multa);
                multas.addElement(multa);
                database.saveData(multa, multas); 
                
                App.displayAlert("La devolución se ha producido fuera de plazo.\nSe ha generado una multa");
            }
        } else {
            App.displayAlert("La devolución ya se ha realizado");
        }
    }
    
    /**
     * Listado de préstamos sin devolver
     */
    public void print()
    {
        print(true, null);
    }
    
    /**
     * Listados de préstamos según tipo
     */
    public void print(Item item)
    {
        print(false, item);
    }
    
    /**
     * Listados
     */
    void print(boolean enprestamo, Item item)
    {       
        Integer contador = 0;
        String title = "";
        
        // Titulo del listado
        if (item != null) {
            title = "Listado de préstamos de " + item.getClass().getName().substring(item.getClass().getName().indexOf(".") + 1) + "s";
        } else {
            title = "Listado de materiales prestados";
        }        
        System.out.println(title);
        System.out.println(new String(new char[title.length()]).replace("\0", "-"));
        
        for (int i = 0; i < getData().getSize(); i++) {
            if (
                (enprestamo && getData().get(i).getFechaDevuelto() == null)         // Materiales actualmente en préstamo
                ||
                (item != null && getData().get(i).getElemento().getClass().equals(item.getClass()))   // Prestamos según tipo de material
                ) {
                ++contador;
                System.out.println(getData().get(i).toString());
            }            
        }
        
        if (contador != 0) {
            System.out.println("\n" + contador.toString() + " registros listados");
        } else {
            System.out.println("\nNo hay préstamos para listar");
        }
        
    }
    
    /**
     * Comprueba si hay préstamos fuera de plazo (del usuario o de todos si es admin)
     */
    public void avisoPrestamosFueraPlazo()
    {        
        String message = "";
        for (Object p : data.toArray()) {
            Prestamo prestamo = (Prestamo)p;
            // Primero compruebo que no se haya devuelto y que haya llegado el día de devolverlo
            if (prestamo.getFechaDevuelto() == null && Period.between(prestamo.getFechaDevolver().toLocalDate(), LocalDateTime.now().toLocalDate()).getDays() >= 0) {
                // Al admin le salen todos los avisos, a los usuarios normales solo los suyos
                if (App.isAdmin() || prestamo.getUsuario().equals(App.getUsuario())) {
                    message += prestamo.toString() + "\n";
                }
            }
        }
       
       if (message != "") {
           App.displayAlert("PRESTAMOS FUERA DE PLAZO\n" + message);                                
       }
    }
}
