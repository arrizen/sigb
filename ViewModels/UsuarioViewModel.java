package ViewModels;

import Models.Usuario;
import Views.Application;


/**
 * Modelo de la vista de los usuarios
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class UsuarioViewModel extends BaseViewModel<Usuario>
{
    /**
     * Constructor
     */
    public UsuarioViewModel(Application app)
    {
        super(app);
    }
    
    /**
     * Implementación del método abstracto de la superclase que devuelve una instancia IAlmacenable 
     */
    public Usuario getType()
    {
        return new Usuario();
    }    
    
    /**
     * Imprime la tarjeta del usuario
     */
    public void print(Usuario usuario)
    {   
        if (usuario != null) {
            String line = "+" + new String(new char[50]).replace("\0", "-") + "+";
            
            System.out.println(line); 
            System.out.println("Tarjeta de usuario de la biblioteca");
            System.out.println("Nº de socio: " + usuario.getCodigo().toString());
            System.out.println("Nombre:" + usuario.getNombre());
            System.out.println(line); 
        }
    }
}
