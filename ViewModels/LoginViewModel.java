package ViewModels;

import Models.Database;
import Models.IAlmacenable;
import Models.Person;
import Models.Usuario;
import Models.Bibliotecario;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;


/**
 * Modelo de vista del login
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class LoginViewModel
{
    
    /**
     * Instancia para controlar el acceso a datos
     */
    private Database<Person> database = new Database<Person>();
    
    /**
     * Para el combo de bibliotecarios
     */
    private DefaultComboBoxModel<Bibliotecario> bibliotecarios = new DefaultComboBoxModel<>();
    
    /**
     * Obtiene la lista de bibliotecarios
     */
    public DefaultComboBoxModel<Bibliotecario> getBibliotecarios() 
    {
        return bibliotecarios;
    }
    
    /**
     * Para el combo de usuarios
     */
    private DefaultComboBoxModel<Usuario> usuarios = new DefaultComboBoxModel<>();
    
    /**
     * Obtiene la lista de usuarios
     */
    public DefaultComboBoxModel<Usuario> getUsuarios() 
    {
        return usuarios;
    }
   
    /**
     * Constructor
     */
    public LoginViewModel()
    {
        loadData();
    }
    
    /**
     * Hace el login
     */
    public boolean login(Person user, char[] password)
    {
        boolean result = false;
        if (java.util.Arrays.equals(user.getPassword().toCharArray(), password)) {
            result = true;
        }
        
        return result;
    }
    
    /**
     * Carga los datos de los usuarios y los bibliotecarios
     */
    private void loadData()
    {
        loadPersons(database.loadData(new Usuario()), usuarios);
        loadPersons(database.loadData(new Bibliotecario()), bibliotecarios);
        
        // Si no hay bibliotecarios se crea el administrador
        if (bibliotecarios.getSize() == 0) {
            createAdminUser();
            loadPersons(database.loadData(new Bibliotecario()), bibliotecarios);
        }
    }
    
    /**
     * Crea el usuario adminstrador
     */
    private void createAdminUser()
    {
        DefaultListModel<Person> data = new DefaultListModel<>();
        Bibliotecario admin = new Bibliotecario();
        admin.setCodigo();
        admin.setNombre("admin");
        admin.setPassword("admin");
        data.addElement(admin);
        database.saveData(admin, data);
    }
    
    /**
     * Carga los datos de usuarios y bibliotecarios en el objeto para el combo
     */
    private void loadPersons(DefaultListModel source, DefaultComboBoxModel target)
    {
        Object[] arr = source.toArray();
        for (int i = 0; i < arr.length; i++) {
            target.addElement((Person)arr[i]);
        }
    }
}
