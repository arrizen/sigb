package ViewModels;

import Models.*;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import Views.Application;

/**
 * Modelo de la vista de las suscripciones
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class SuscripcionViewModel extends BaseViewModel<Suscripcion>
{
    
    /**
     * Constructor
     */
    public SuscripcionViewModel(Application app)
    {
        super(app);
    }
    
    /**
     * Implementación del método abstracto de la superclase que devuelve una instancia IAlmacenable 
     */
    public Suscripcion getType()
    {
        return new Suscripcion();
    }
    
    
    /**
     * Lista de periodicidades para la vista
     */
    public DefaultComboBoxModel<String> getPeriodicidadList()
    {
        return Periodicidad.getPeriodicidadList();
    }
    
    /**
     * Lista de items suscribibles
     */
    public DefaultComboBoxModel<ISuscribible> getItemsSuscribiblesList()
    {
        DefaultComboBoxModel<ISuscribible> result = new DefaultComboBoxModel<>();
        
        for (ISuscribible suscribible : getUniqueList()) {
            result.addElement(suscribible);
        }        
        
        // Asegurar el acceso si no hay items suscribibles
        if (result.getSize() == 0) {
            Revista r = new Revista();
            r.setTitle("No hay materiales suscribibles");
            result.addElement(r);
        }
        return result;
    }    
    
    /**
     * Obtiene una lista única (por nombre) del elemento suscribible 
     */
    ArrayList<ISuscribible> getUniqueList ()
    {
        Object[] arr; 
        ArrayList<ISuscribible> result = new ArrayList<>();
        
        for (Integer it = 0; it < 2; it++){
            if (it.equals(1))
                arr = new Database<IAlmacenable>().loadData(new Revista()).toArray();
            else
                arr = new Database<IAlmacenable>().loadData(new Periodico()).toArray();
            List list = Arrays.asList(arr);     
            for (Object i : list) {
                boolean existe = false;
                ISuscribible suscribible = (ISuscribible)i;
                
                // Comprobar si el elemento ya está en la lista de retorno (se busca por título y se da por hecho que no hay dos item suscribibles con el mismo título)
                for (ISuscribible item : result) {
                    if (item.getTitle().equals(suscribible.getTitle())) {
                        existe = true;
                    }
                }
                
                // Se añade a la lista de retorno
                if (!existe) {
                    result.add(suscribible);
                }
            }
        }
        return result;
    }
}
