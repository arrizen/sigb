package ViewModels;

import javax.swing.DefaultListModel;
import Models.Database;
import Models.IAlmacenable;
import Models.Item;
import Models.Gender;
import Models.Prestamo;
import Models.Reserva;
import Models.Person;
import javax.swing.DefaultComboBoxModel;
import java.time.LocalDateTime;
import java.time.Period;
import Views.Application;

/**
 * Modelo de vista base
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public abstract class BaseViewModel<IAlmacenable>
{
    /**
     * Para saber si es el viewmodel de préstamos
     */
    protected boolean isprestamovm;    
    public boolean IsPrestamoVM() {
        return isprestamovm;
    }
    
    /**
     * Para saber si es el viewmodel de reservas
     */
    protected boolean isreservavm;    
    public boolean IsReservaVM() {
        return isreservavm;
    }
    
    /**
     * Acceso a la aplicación principal
     */
    public Application App;
    
    /**
     * Instancia para controlar la lectura y escritura de los datos
     */
    protected Database<IAlmacenable> database = new Database<IAlmacenable>();
    
     /**
     * Lista para mostrar en el JList
     */
    protected DefaultListModel<IAlmacenable> data;        
    public DefaultListModel<IAlmacenable> getData() {  return data; }
    public DefaultListModel<IAlmacenable> getVisibleData() {  return data; }
    
     /**
     * Lista para los préstamos
     */
    protected DefaultListModel<IAlmacenable> prestamos;        
    public DefaultListModel<IAlmacenable> getPrestamos() {  return prestamos; }

    /**
    * Lista para la gestión de las reservas
    */
    protected DefaultListModel<IAlmacenable> reservas;        
    public DefaultListModel<IAlmacenable> getReservas() {  return reservas; }
    
    /**
     * Este método se implementará en las subclases y retornará una instancia de la clase IAlmacenable concreta con
     * la que trabajará el Viewmodel (Gender, Libro, etc.)
     * Se usará tanto en los métodos loadData como saveData de Database para obtener el nombre de la clase.
     */
    abstract public IAlmacenable getType();

    /**
     * Constructor
     */
    public BaseViewModel (Application app)
    {
        App = app;
        data = new DefaultListModel<IAlmacenable>();
        prestamos = new DefaultListModel<IAlmacenable>();
        reservas = new DefaultListModel<IAlmacenable>();
        loadData();
    }
    
    /**
     * Carga los datos
     */
    protected void loadData()
    {
        data = database.loadData(getType());
        prestamos = database.loadData((IAlmacenable)new Prestamo());
        reservas = database.loadData((IAlmacenable)new Reserva());
    }
    
    /**
     * Almacena los datos
     */
    public void saveData()
    {
        database.saveData(getType(), data);
        if (!IsPrestamoVM()) {
            database.saveData((IAlmacenable)new Prestamo(), prestamos);
        }
        if (!IsReservaVM()) {
            database.saveData((IAlmacenable)new Reserva(), reservas);
        }
    }
    
     /**
     * Inserta un nuevo género
     */
    public void insert(IAlmacenable x)
    {
        // Si x es un item, se pone disponible para su préstamo
        if (x instanceof Item) {
            Item item = (Item)x;
            item.setDisponible(true);
            
            // Si el ISBN no se rellena se asignar automáticamente el hashcode
            if (item.getIsbn() == null || item.getIsbn().trim().length() == 0) {
                item.setIsbn(((Integer)item.hashCode()).toString());
            }
        }
        
        data.addElement(x);
        saveData();
    }
    
    /**
     * Elimina un género de la lista
     */
    public boolean delete(IAlmacenable x)
    {
        boolean result = data.removeElement(x);
        saveData();
        return result;
    }
    
    /**
     * Devuelve los datos para el picker de géneros
     */
    public DefaultComboBoxModel<Gender> getGenderList()
    {
        return new GenderViewModel(App).getGenderList();
    }    

    /**
     * Comprueba la disponibilidad de un item
     */
    public boolean checkDisponibilidad(Item item) { return checkDisponibilidad(item, true); }
    public boolean checkDisponibilidad(Item item, boolean warnings)
    {
        boolean result = item.getDisponible();
        if (result) {
            // Comprobar si el elemento se encuentra reservado
            if (estaReservado(item)) {
                result = false;
                if (warnings) App.displayAlert("El elemento reservado");
            } else {
                if (warnings) App.displayAlert("El elemento está disponible para su préstamo");
            }
            
        } else {
            if (warnings) App.displayAlert("El elemento se encuentra actualmente prestado");
        }                 
        
        return result;
    }
    
    /**
     * Comprueba si un elemento está reservado por otro usuario
     */
    public boolean estaReservado(Item item)
    {
        boolean result = false;
        
        for (Object o : reservas.toArray()) {
            Reserva reserva = (Reserva)o;
            if (!reserva.getUsuario().equals(App.getUsuario()) && item.equals(reserva.getElemento())) {
                result = true;
                break;
            }
        }
        return result;
    }
    
    /**
     * Comprueba que el usuario no ha superado el número de préstamos máximos pendinetes de devolver
     */
    public boolean checkUsuario(Person usuario)
    {
        int contador = 0;
        boolean result;
        // Contar los prestamos pendientes que tiene el usuario
        for (int i = 0; i < getPrestamos().getSize(); ++i) {
            Prestamo p = (Prestamo)getPrestamos().get(i);
            if (p.getFechaDevuelto() == null && p.getUsuario().equals(usuario)) {
                ++contador;
            }
        }        
        result = (contador < 6);
        if (!result) {
            App.displayAlert("El usuario ha superado el número de préstamos pendientes");
        }
        return result;
    }
    
    /**
     * Realiza el préstamo de un elemento de una colección
     */
    public boolean solicitarPrestamo(Person usuario, Item item)
    {
        // Primero se chequea la disponiblidad del elemento
        if (!checkDisponibilidad(item, false)) {
            App.displayAlert("El elemento se encuentra actualmente prestado o reservado");
            return false;
        }
        
        // Ahora se comprueba que el usuario no ha superado el número de items que se pueden tener en préstamo
        if (!checkUsuario(usuario)) {
            return false;
        }
        item.setDisponible(false);
        Prestamo prestamo = new Prestamo();
        prestamo.setUsuario(usuario);
        prestamo.setElemento(item);
        prestamo.setFechaPrestamo(LocalDateTime.now());
        prestamo.setFechaDevolver(LocalDateTime.now().plus(Period.ofDays(15)));
        prestamos.addElement((IAlmacenable)prestamo);
        
        // Anula la reserva del elemento si la hay
        anularReserva(item);
        
        saveData();
        App.displayAlert("La devolución debe hacerse antes del " + prestamo.getFechaDevolver().toString().substring(0, 10));        
        return true;
    }
    
    /**
     * Si hay una reserva del elemento por el usuario, se elimina
     */
    void anularReserva(Item item)
    {
        for (int i = 0; i < getReservas().getSize(); ++i) {
            Reserva reserva = (Reserva)getReservas().get(i);
            if (reserva.getUsuario().equals(App.getUsuario()) && reserva.getElemento().equals(item)) {
                getReservas().remove(i);
            }
        }
    }
    
    /**
     * Solicitar la reserva de un elemento
     */
    public void reservar(Person usuario, Item item)
    {        
        // Primero se chequea la disponiblidad del elemento
        if (checkDisponibilidad(item, false)) {
            App.displayAlert("El elemento está disponible. No se puede reservar");
            return;
        }
        
        // Comprueba que el elemento no esté ya reservado
        if (reservado(item)) {
            App.displayAlert("Ya hay una reserva hecha. No se puede reservar");
            return;
        }
        
        // Realizar la reserva
        Reserva reserva = new Reserva();
        reserva.setFecha(LocalDateTime.now());
        reserva.setUsuario(usuario);
        reserva.setElemento(item);
        getReservas().addElement((IAlmacenable)reserva);
        saveData();
        App.displayAlert("La reserva se ha realizado correctamente");
    }
    
    /**
     * Comprueba si un elemento ya está reservado
     */
    boolean reservado(Item item)
    {
        boolean result = false;
        for (int i = 0; i < getReservas().getSize(); ++i) {
            Reserva reserva = (Reserva)getReservas().get(i);
            if (reserva.getElemento().equals(item)) {
                result = true;
                break;
            }
        }        
        return result;
    }
}
