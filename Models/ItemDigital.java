package Models;

/**
 * Abstract class ItemDigital - Items que son digitales (audio, video)
 * 
 * @author: Rafa León 
 * Date: (@2016)
 */
public abstract class ItemDigital extends Item
{
    public ItemDigital ()
    {
        setDigital (true);
    }
    
    /**
     * Tamaño del elemento en Mb.
     */
    private Integer size;
    public void setSize(Integer mb) { size = mb; }
    public Integer getSize() { return size; }
    
    /**
     * Duración del elemento en minutos
     */
    private Integer duration;
    public void setDuration(Integer min) { duration = min; }
    public Integer getDuration() { return duration; }
    
    /**
     * Lo que se mostrará en la lista de audios y videos
     */
    public String toString()
    {
        return this.getTitle() + " (" + this.getGenero().getName() + "). Duración " + getDuration().toString() + " minutos.";
    }
}


