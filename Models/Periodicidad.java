package Models;

import javax.swing.DefaultComboBoxModel;


/**
 * Enum para la periodicidad de las suscripciones
 * 
 * @author (Rafa Leon)
 * @version (@2016)
 */
public enum Periodicidad
{
    DIARIA, SEMANAL, MENSUAL, BIMESTRAL, TRIMESTRAL, SEMESTRAL, ANUAL;
    
    /**
     * Obtiene una lista con las diferentes periodicidades para usarlas en lun JComboBox
     */
    public static DefaultComboBoxModel<String> getPeriodicidadList()
    {
        DefaultComboBoxModel<String> list = new DefaultComboBoxModel<>();        
        
        for (Periodicidad p : Periodicidad.values()) {
            list.addElement(p.toString());
        }
        
        return list;
    }
}
