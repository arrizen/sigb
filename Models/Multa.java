package Models;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.time.Period;

/**
 * Implementación de la clase de las multas
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class Multa implements IAlmacenable, Serializable
{
    /**
     * Prestamo que generó la multa
     */
    Prestamo prestamo;
    public Prestamo getPrestamo() { return prestamo; }
    public void setPrestamo(Prestamo p) { prestamo = p; }
    
    /**
     * Fecha en que se cancela la multa
     */
    LocalDateTime fechaCancelacion;
    public LocalDateTime getFechaCancelacion() { return fechaCancelacion; }
    public void setFechaCancelacion(LocalDateTime d) { fechaCancelacion = d; }
    
    /**
     * Obtiene el usuario que recibe la multa (el del préstamo)
     */
    public Person getUsuario() 
    {
        return getPrestamo().getUsuario();
    }
    
    /**
     * Obtiene el item del préstamo
     */
    public Item getItem()
    {
        return getPrestamo().getElemento();
    }
    
    /**
     * Obtiene los días de retraso del préstamo
     */
    public Integer getRetraso()
    {
        return Period.between(getPrestamo().getFechaDevolver().toLocalDate(), getPrestamo().getFechaDevuelto().toLocalDate()).getDays();
    }
    
    /**
     * Lo que se va a ver en las listas
     */
    public String toString()
    {
        String estado = "Pendiente";
        if (getFechaCancelacion() != null) {
            estado = "Pagada el " + getFechaCancelacion().toString().substring(0, 10);
        }
        return getUsuario().getNombre() + " - " + getItem().getTitle() + " (" + getRetraso().toString() + " días de retraso) [" + estado + "]";
    }
}
