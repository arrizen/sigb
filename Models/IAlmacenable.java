package Models;


/**
 * Interface de las clases que van a actuar como registros almacenables
 * No obliga a implementar nada, se usa para tener el polimorfismo de todos
 * los materiales de la coleccion y tratarlos de forma unica en la lectura y 
 * escritura de datos
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */

public interface IAlmacenable 
{
}
