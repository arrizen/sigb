package Models;


/**
 * Clase video
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class Video extends ItemDigital
{
    /**
     * Describe el video
     */
    public String describe ()
    {
        return getIsbn() + "-" + "Video. " + getTitle() + " (" + getEditorial() + "). Género: " + genero.getName() + ". Tamaño: " + getSize().toString() + " Mb. Duración: " + getDuration().toString() + " mins.";
    }
 
}
