package Models;


/**
 * Clase periodico
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class Periodico extends ItemAnalogico implements ISuscribible
{
    /**
     * Número de periodico. Obligatorio al ser ISuscribible
     */
    Integer numero;
    public Integer getNumero() { return numero;}
    public void setNumero(Integer n) { numero = n; }
    
    /**
     * Lo que se mostrará en la lista de periodicos
     */
    public String toString()
    {
        return getIsbn() + "-" + getTitle() + " (" + getGenero().getName() + ") nº " + getNumero().toString();
    }
    
    /**
     * Describe el periodico
     */
    public String describe ()
    {
        String result = "";         
        result = "Periodico. " + getTitle() + " (" + getEditorial() + ") nº " + getNumero().toString() + ". Género: " + genero.getName();
        return result;
    }
    
}
