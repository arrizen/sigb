package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.BaseViewModel;
import ViewModels.SuscripcionViewModel;
import Models.Suscripcion;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
 * Vista de las suscripciones
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class SuscripcionesPage extends ViewBase
{
    /**
     * Contructor
     */
    public SuscripcionesPage(Application app)
    {
        super(app);
    }
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new SuscripcionViewModel(App);
    }
    
    /**
     * Título de la ventana
     */
    String getTitlePage()
    {
        return "Suscripciones";
    }
    
    
    /**
     * Inicializar componentes de la pantalla
     */
    void initializeComponents()
    {
        configure();

        configureMenu();
        configureList();
    }
    
}
