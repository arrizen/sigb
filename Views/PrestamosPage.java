package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.BaseViewModel;
import ViewModels.PrestamoViewModel;
import Models.*;
import Views.ViewBase;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
 * Vista de las suscripciones
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class PrestamosPage extends ViewBase
{
    /**
     * Contructor
     */
    public PrestamosPage(Application app)
    {
        super(app);
    }
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new PrestamoViewModel(App);
    }
    
    /**
     * Título de la ventana
     */
    String getTitlePage()
    {
        return "Préstamos";
    }
    
    
    /**
     * Inicializar componentes de la pantalla
     */
    void initializeComponents()
    {
        configure();

        configureMenu();
        configureList();
    }
    
    /**
     * Configurar el menú de la opción
     */
    protected void configureMenu()
    {
        JMenuBar barra = new JMenuBar();
        setJMenuBar(barra);
        
        JMenu menu = new JMenu("Préstamos");
        barra.add(menu);
        
        JMenuItem item;
        item = new JMenuItem("Realizar devolución");
        item.addActionListener(new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                if (list.getSelectedValue() != null) {
                    ((PrestamoViewModel)viewModel).realizarDevolucion((Prestamo)list.getSelectedValue());                    
                }
            }
        });     
        menu.add(item);
        
        // Los bibliotecarios pueden sacar listados de los préstamos
        if (App.isAdmin()) {
            menu = new JMenu("Listados");
            barra.add(menu);            

            item = new JMenuItem("Materiales prestados");
            item.addActionListener(new ActionListener () {
                public void actionPerformed(ActionEvent e) {
                    ((PrestamoViewModel)viewModel).print();
                }
            });     
            
            menu.add(item);
            item = new JMenuItem("Préstamos de libros");
            item.addActionListener(new ActionListener () {
                public void actionPerformed(ActionEvent e) {
                    ((PrestamoViewModel)viewModel).print(new Libro());
                }
            });     
            menu.add(item);
            
            item = new JMenuItem("Préstamos de revistas");
            item.addActionListener(new ActionListener () {
                public void actionPerformed(ActionEvent e) {
                    ((PrestamoViewModel)viewModel).print(new Revista());
                }
            });     
            menu.add(item);
            
            item = new JMenuItem("Préstamos de periódico");
            item.addActionListener(new ActionListener () {
                public void actionPerformed(ActionEvent e) {
                    ((PrestamoViewModel)viewModel).print(new Periodico());
                }
            });     
            menu.add(item);
            
            item = new JMenuItem("Préstamos de audio");
            item.addActionListener(new ActionListener () {
                public void actionPerformed(ActionEvent e) {
                    ((PrestamoViewModel)viewModel).print(new Audio());
                }
            });     
            menu.add(item);
            
            item = new JMenuItem("Préstamos de video");
            item.addActionListener(new ActionListener () {
                public void actionPerformed(ActionEvent e) {
                    ((PrestamoViewModel)viewModel).print(new Video());
                }
            });     
            menu.add(item);
        }
    }
    
}
