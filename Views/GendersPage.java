package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.BaseViewModel;
import ViewModels.GenderViewModel;
import Models.Gender;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Vista de los géneros
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class GendersPage extends ViewBase
{
    /**
     * Contructor
     */
    public GendersPage(Application app)
    {
        super(app);
    }
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new GenderViewModel(App);
    }
    
    /**
     * Título de la ventana
     */
    String getTitlePage()
    {
        return "Géneros";
    }
    
    /**
     * Inicializar componentes de la pantalla
     */
    void initializeComponents()
    {
        configure();

        configureMenu();
        configureList();
    }    
    
}
