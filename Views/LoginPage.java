package Views;

import javax.swing.*;
import java.awt.*;
import Models.Person;
import ViewModels.LoginViewModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
 * Página del login
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class LoginPage extends JFrame
{    
    /**
     * Combo con la lista de usuarios
     */
    JComboBox userList;
    
    /**
     * Entry para el password
     */
    JPasswordField passwordEntry;
    
    /**
     * Para saber si el login es correcto o no
     */
    boolean ok = false;
    
    /**
     * Constructor
     */
    public LoginPage ()
    {        
        super();
        
        initializeComponents();
    }
    
    /**
     * Para saber si se ha hecho el login correctamente o no
     */
    public boolean loginOk()
    {
        return ok;
    }
    
    /**
     * Viewmodel del login
     */
    protected LoginViewModel viewModel = new LoginViewModel();
    
    /**
     * Inicializar componentes de la pantalla
     */
    protected void initializeComponents()
    {
        // Configurar la pantalla
        configure();
        
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        getContentPane().add(panel);
        
        JLabel label = new JLabel();
        label.setText("Tipo de acceso");
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(label);
        
        // Combo para elegir cómo logarse
        String[] tipos = {"Usuario normal", "Bibliotecario o administrador"};
        final JComboBox combo = new JComboBox(tipos);
        combo.addActionListener(new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                if (combo.getSelectedIndex() == 1) {
                    userList.setModel(viewModel.getBibliotecarios());
                } else {
                    userList.setModel(viewModel.getUsuarios());
                }
            }
        });        
        panel.add(combo);
        
        label = new JLabel();
        label.setText("Usuario");
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(label);
        
        // Combo para elegir el usuario o bibliotecario
        userList = new JComboBox();
        userList.setModel(viewModel.getUsuarios());
        panel.add(userList);
        
        label = new JLabel();
        label.setText("Contraseña");
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(label);
        
        // Entry para la contraseña
        passwordEntry = new JPasswordField(20);
        panel.add(passwordEntry);
        
        // Botó para entrar
        JButton button = new JButton();
        button.setText("Entrar");
        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        button.addActionListener(new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                if (passwordEntry.getPassword() != null) {
                    if (viewModel.login((Person)userList.getSelectedItem(), passwordEntry.getPassword()) ) {
                        Application page = new Application((Person)userList.getSelectedItem());
                        page.setVisible(true);
                        dispose();
                    } else {
                        JOptionPane.showMessageDialog(null, "La contraseña no es correcta");                    
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Debe introducir la contraseña");                    
                }
            }
        });        
        panel.add(button);
        
    }
    
    /**
     * Configurar la ventana
     */
    protected void configure()
    {
        setTitle("Login");
        setSize(300, 180);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);  
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                dispose();        
            }
        });
    }
}
