package Views;

import javax.swing.*;
import ViewModels.RevistaViewModel;
import Models.Revista;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
 * Vista de los revistas
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class RevistasPage extends ViewBase
{
    /**
     * Contructor
     */
    public RevistasPage(Application app)
    {
        super(app);
    }
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new RevistaViewModel(App);
    }
    
    /**
     * Título de la ventana
     */
    String getTitlePage()
    {
        return "Revistas";
    }
    
    
    /**
     * Inicializar componentes de la pantalla
     */
    void initializeComponents()
    {
        configure();

        configureMenu();
        configureList();
    }
    
}
