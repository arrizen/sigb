package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.BaseViewModel;
import Models.Person;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import Util.SigbUtilities;


/**
 * Nueva persona
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public abstract class NewPersonPage extends ViewBase
{
    /**
     * Contructor
     */
    NewPersonPage(Application app)
    {
        super(app);
    }
    
    /**
     * Panel para los entries
     */
    protected JPanel panel;
    /**
     * Campos para la toma de datos
     */
    protected JTextField nameText;
    protected JTextField passwordText;
    
    /**
     * Para controlar el número de elemento en el SpringLayout
     */
    protected int numPairs;
  
    /**
     * Método a implementar en las sub-clases para preparar el item para almacenarlo
     */
    abstract Person preparePerson();
    
    /**
     * Implementar en las sub-clases el tamaño de la pantalla
     */
    protected void setSizePage()
    {
        setSize(500, 130);
    }
    
    /**
     * Inicializar componentes de la pantalla
     */
    void initializeComponents()
    {
        configure();
        configureFieldsData();
    }
    
    /**
     * Configurar la ventana
     */
    @Override
    protected void configure()
    {
        setTitle(getTitlePage());
        setSizePage();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);  
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                dispose();        
            }
        });
    }
    
    /**
     * Configura los entries comunes de los items analogicos
     */
    protected void configureFieldsData()
    {
        numPairs = 0;
        JLabel label;
        
        panel = new JPanel(new SpringLayout());
        getContentPane().add(panel);
        
        label = new JLabel("Nombre: ", JLabel.TRAILING);
        nameText = new JTextField(10);
        label.setLabelFor(nameText);
        panel.add(label);
        panel.add(nameText);
        ++numPairs;
        
        label = new JLabel("Password: ", JLabel.TRAILING);
        passwordText = new JTextField(10);
        label.setLabelFor(passwordText);
        panel.add(label);
        panel.add(passwordText);
        ++numPairs;
        
        putButtons();
        
        SpringUtilities.makeCompactGrid(panel,
            numPairs, 2, //rows, cols
            6, 6,        //initX, initY
            6, 6);       //xPad, yPad        
    }    
    
    /**
     * Poner los botones de aceptar y cancelar
     */
    protected void putButtons()
    {
        // Botón para almacenar
        JButton okButton = new JButton("Ok");
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {     
                if (validateFields()) {
                    
                    // Pasar el person al viewmodel para que lo inserte
                    viewModel.insert(preparePerson());
                    
                    // Limpiar los campos
                    clearEntries();
                }
            }
        });               
        
        // Botón para anular
        JButton cancelButton = new JButton("Cancelar");
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {     
                // Limpiar los campos
                clearEntries();
            }
        });               
        panel.add(okButton);
        panel.add(cancelButton);
        ++numPairs;        
    }
  
    /**
     * Valida la toma de datos
     */
    protected boolean validateFields()
    {
        boolean result = true;
        
        if (nameText.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe introducir el nombre");
            result = false;
        } else if (passwordText.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe introducir la clave");
            result = false;
        } 
        return result;
    }    
    
    /**
     * Borrar las cajas de texto
     */
    protected void clearEntries()
    {
        nameText.setText("");
        passwordText.setText("");
    }
}
