package Views;

import javax.swing.*;
import ViewModels.LibroViewModel;
import Models.Libro;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
 * Vista de los libros
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class LibrosPage extends ViewBase
{
    /**
     * Contructor
     */
    public LibrosPage(Application app)
    {
        super(app);
    }
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new LibroViewModel(App);
    }
    
    /**
     * Título de la ventana
     */
    String getTitlePage()
    {
        return "Libros";
    }
    
    
    /**
     * Inicializar componentes de la pantalla
     */
    void initializeComponents()
    {
        configure();

        configureMenu();
        configureList();
    }
    
}
