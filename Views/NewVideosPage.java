package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.VideoViewModel;
import Models.Video;
import Models.Gender;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import Util.SigbUtilities;


/**
 * Nuevo video
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class NewVideosPage extends NewItemDigitalPage
{
    /**
     * Contructor
     */
    public NewVideosPage(Application app)
    {
        super(app);
    }
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new VideoViewModel(App);
    }
    
    /**
     * Título de la página
     */
    String getTitlePage()
    {
        return "Nuevo video";
    }    
    
    /**
     * Preparar el item para almacenarlo
     */
    protected Video prepareItem()
    {
        // Generar un nuevo item y darle los valores de los Entries
        Video item = (Video)viewModel.getType();
        item.setIsbn(isbnText.getText());
        item.setTitle(nameText.getText());
        item.setEditorial(editorialText.getText());
        item.setGenero((Gender)genderPicker.getSelectedItem());
        item.setDuration(Integer.parseInt(durationText.getText()));
        item.setSize(Integer.parseInt(sizeText.getText()));
        return item;
    }
    
}
