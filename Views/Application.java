package Views;

import javax.swing.*;
import Controllers.MenuController; 
import Models.Person;
import Models.Bibliotecario;
import ViewModels.PrestamoViewModel;
import ViewModels.ReservaViewModel;

/**
 * Clase principal de la aplicación
 * 
 * @author (Rafa León)
 * @version (@2016)
 */
public class Application extends JFrame
{
    /**
     * Usuario que se ha logado en la aplicación
     */
    Person usuario;
    /**
     * Obtiene el usuario que se ha logado en la aplicación
     */
    public Person getUsuario() 
    {
        return usuario;
    }
    
    /**
     * Establece el usuario que se ha logado en la aplicación
     */
    public void setUsuario(Person u)
    {
        usuario = u;
    }
    
    /**
     * Obtiene si el usuario que se ha logado es un admin o un usuario normal
     */
    public boolean isAdmin()
    {
        return (getUsuario() instanceof Bibliotecario);
    }
         
    /**
     * Controlador del menú
     */
    MenuController controlador = new MenuController(this);
    
    /**
     * Constructor
     */
    public Application(Person user)
    {
        super();  
        
        setUsuario(user);
        
        initializeComponets();
        
        avisoPrestamosFueraPlazo();
        
        avisoReservasEnPrestamo();
        
        avisoReservasDisponible();
    }   
    
    /**
     * Muestra un mensaje
     */
    public void displayAlert(String message)
    {
        JOptionPane.showMessageDialog(null, message);
    }
    
    /**
     * Llama al vm de préstamos para mostrar el aviso fuera de plazo
     */
    void avisoPrestamosFueraPlazo()
    {
        PrestamoViewModel vm = new PrestamoViewModel(this);
        vm.avisoPrestamosFueraPlazo();
    }
    
    
    /**
     * Llama al vm de reservas para mostrar el aviso si el usuario tiene algún elemento en préstamo que alguien ha reservado
     */
    void avisoReservasEnPrestamo()
    {
        ReservaViewModel vm = new ReservaViewModel(this);
        vm.avisoReservasEnPrestamo();
    }
    
    /**
     * Llama al vm de reservas para mostrar un aviso si hay algún elemento de los que el usuario tiene reservados disponible
     */
    void avisoReservasDisponible()
    {
        ReservaViewModel vm = new ReservaViewModel(this);
        vm.avisoReservasDisponible();
    }
    
    /**
     * Inicializar los componentes de la pantalla
     */
    private void initializeComponets()
    {
        configure();  

        mainMenu();
        
    }
    
    /**
     * Configurar la ventana principal
     */
    private void configure()
    {
        setLayout(null);
        setTitle("Sigb");
        
        setSize(800, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    
    /**
     * Poner el menú principal
     */
    private void mainMenu()
    {        
        JMenuBar barra = new JMenuBar();
        setJMenuBar(barra);
        
        coleccionMenu(barra);
        
        if (isAdmin()) {
            usuariosMenu(barra);
        }
    }   
    
    /**
     * Menu de control de acceso de los usuarios
     */
    private void usuariosMenu(JMenuBar barra)
    {
        JMenu menu;
        JMenu submenu;
        JMenuItem item;
        
        // Menú principal
        menu = new JMenu("Control de acceso");
        barra.add(menu);     
        
        // Menú de usuarios
        submenu = new JMenu("Usuarios");
        menu.add(submenu);
        
        item = new JMenuItem("Lista de usuarios");
        item.setActionCommand("_LISTA_USUARIOS_");
        item.addActionListener(controlador);
        submenu.add(item);
        
        item = new JMenuItem("Nuevo usuario");
        item.setActionCommand("_NUEVO_USUARIO_");
        item.addActionListener(controlador);
        submenu.add(item);
        
        // Menú de bibliotecarios
        submenu = new JMenu("Bibliotecarios");
        menu.add(submenu);
        
        item = new JMenuItem("Lista de bibliotecarios");
        item.setActionCommand("_LISTA_BIBLIOTECARIOS_");
        item.addActionListener(controlador);
        submenu.add(item);
        
        item = new JMenuItem("Nuevo bibliotecario");
        item.setActionCommand("_NUEVO_BIBLIOTECARIO_");
        item.addActionListener(controlador);
        submenu.add(item);
    }
    
    /**
     * Menú colecciones
     */
    private void coleccionMenu(JMenuBar barra)
    {
        JMenu menu;
        JMenu submenu;
        JMenuItem item;
        
        // Menú colecciones
        menu = new JMenu("Colecciones");
        barra.add(menu);
        
        // Opción de búsqueda sencilla
        item = new JMenuItem ("Búsqueda sencilla");
        item.setActionCommand("_QUICK_SEARCH_");
        item.addActionListener(controlador);
        menu.add(item);
        
        // Opción de búsqueda flexible
        item = new JMenuItem ("Búsqueda flexible");
        item.setActionCommand("_SEARCH_");
        item.addActionListener(controlador);
        menu.add(item);
        
        // Préstamos
        item = new JMenuItem ("Préstamos");
        item.setActionCommand("_PRESTAMOS_");
        item.addActionListener(controlador);
        menu.add(item);
        
        // Préstamos
        item = new JMenuItem ("Reservas");
        item.setActionCommand("_RESERVAS_");
        item.addActionListener(controlador);
        menu.add(item);
        
        // Préstamos
        item = new JMenuItem ("Multas");
        item.setActionCommand("_MULTAS_");
        item.addActionListener(controlador);
        menu.add(item);
        
        // Menú de géneros
        submenu = new JMenu("Géneros");
        menu.add(submenu);
        
        item = new JMenuItem("Lista de géneros");
        item.setActionCommand("_LISTA_GENEROS_");
        item.addActionListener(controlador);
        submenu.add(item);
        
        if (isAdmin()) {
            item = new JMenuItem("Nuevo género");
            item.setActionCommand("_NUEVO_GENERO_");
            item.addActionListener(controlador);
            submenu.add(item);
        }
        
        // Menú de libros
        submenu = new JMenu("Libros");
        menu.add(submenu);
        
        item = new JMenuItem("Lista de libros");
        item.setActionCommand("_LISTA_LIBROS_");
        item.addActionListener(controlador);
        submenu.add(item);
        
        if (isAdmin()) {
            item = new JMenuItem("Nuevo libro");
            item.setActionCommand("_NUEVO_LIBRO_");
            item.addActionListener(controlador);
            submenu.add(item);
        }
        
        // Menú de revistas
        submenu = new JMenu("Revistas");
        menu.add(submenu);
        
        item = new JMenuItem("Lista de revistas");
        item.setActionCommand("_LISTA_REVISTAS_");
        item.addActionListener(controlador);
        submenu.add(item);
        
        if (isAdmin()) {
            item = new JMenuItem("Nueva revista");
            item.setActionCommand("_NUEVO_REVISTA_");
            item.addActionListener(controlador);
            submenu.add(item);
        }
        
        // Menú de periódicos
        submenu = new JMenu("Periódicos");
        menu.add(submenu);
        
        item = new JMenuItem("Lista de periódicos");
        item.setActionCommand("_LISTA_PERIODICOS_");
        item.addActionListener(controlador);
        submenu.add(item);
        
        if (isAdmin()) {
            item = new JMenuItem("Nuevo periódico");
            item.setActionCommand("_NUEVO_PERIODICO_");
            item.addActionListener(controlador);
            submenu.add(item);
        }
        
        
        // Menú de suscripciones
        if (isAdmin()) {
            submenu = new JMenu("Gestionar suscripciones");
            menu.add(submenu);
            
            item = new JMenuItem("Suscripciones activas");
            item.setActionCommand("_LISTA_SUSCRIPCIONES_");
            item.addActionListener(controlador);
            submenu.add(item);
            
            item = new JMenuItem("Nueva sucripción");
            item.setActionCommand("_NUEVA_SUSCRIPCION_");
            item.addActionListener(controlador);
            submenu.add(item);
        }
        
         // Menú de audios
        submenu = new JMenu("Audios");
        menu.add(submenu);
        
        item = new JMenuItem("Lista de audios");
        item.setActionCommand("_LISTA_AUDIOS_");
        item.addActionListener(controlador);
        submenu.add(item);
        
        if (isAdmin()) {
            item = new JMenuItem("Nuevo audio");
            item.setActionCommand("_NUEVO_AUDIO_");
            item.addActionListener(controlador);
            submenu.add(item);
        }
        
         // Menú de videos
        submenu = new JMenu("Videos");
        menu.add(submenu);
        
        item = new JMenuItem("Lista de videos");
        item.setActionCommand("_LISTA_VIDEOS_");
        item.addActionListener(controlador);
        submenu.add(item);
        
        if (isAdmin()) {
            item = new JMenuItem("Nuevo video");
            item.setActionCommand("_NUEVO_VIDEO_");
            item.addActionListener(controlador);
            submenu.add(item);
        }
    }

}