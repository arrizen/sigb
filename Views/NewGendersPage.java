package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.GenderViewModel;
import Models.Gender;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
 * Nuevo género
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class NewGendersPage extends ViewBase
{
    /**
     * Constructor
     */
    public NewGendersPage(Application app)
    {
        super(app);
    }
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new GenderViewModel(App);
    }
    
    /**
     * Título de la página
     */
    String getTitlePage()
    {
        return "Nuevo género";
    }
    
    /**
     * Inicializar componentes de la pantalla
     */
    void initializeComponents()
    {
        configure();
        configureFieldsData();
    }
    
    /**
     * Configurar la ventana
     */
    @Override
    protected void configure()
    {
        setTitle(getTitlePage());
        setSize(500, 180);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);  
        setLayout(new GridLayout(1, 2));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                dispose();        
            }
        });
    }
    
    private void configureFieldsData()
    {
        JLabel  namelabel= new JLabel("Nombre: ", JLabel.RIGHT);
        final JTextField nameText = new JTextField(25);

        JButton okButton = new JButton("Ok");
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {     
                if (!nameText.getText().isEmpty()) {
                    Gender g = new Gender();
                    g.setName(nameText.getText());
                    viewModel.insert(g);
                    nameText.setText("");
                } else {
                    JOptionPane.showMessageDialog(null, "Debe introducir el nombre del género");
                }
            }
        });
        
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        getContentPane().add(panel);

        panel.add(namelabel);
        panel.add(nameText);
        panel.add(okButton);
    }    
    
}
