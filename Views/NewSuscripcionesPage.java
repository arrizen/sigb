package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.SuscripcionViewModel;
import Models.Suscripcion;
import Models.ISuscribible;
import Models.Periodicidad;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
 * Nueva suscripción
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class NewSuscripcionesPage extends ViewBase
{
    
    /**
     * Contructor
     */
    public NewSuscripcionesPage(Application app)
    {
        super(app);
    }
    
    /**
     * Panel para los entries
     */
    protected JPanel panel;
    
    /**
     * Campos para la toma de datos
     */
    protected JComboBox periodicidadPicker;
    protected JComboBox suscribiblePicker;
    
    /**
     * Para controlar el número de elemento en el SpringLayout
     */
    protected int numPairs;
    
    void createViewModel()
    {
        viewModel = new SuscripcionViewModel(App);
    }
    
    /**
     * Título de la página
     */
    String getTitlePage()
    {
        return "Nueva suscripción";
    }
    
    /**
     * Tamaño de la ventana
     */
    void setSizePage()
    {
        setSize(500, 220);
    }
    
    /**
     * Inicializar componentes de la pantalla
     */
    void initializeComponents()
    {
        configure();
        configureFieldsData();
    }
    
    /**
     * Configurar la ventana
     */
    @Override
    protected void configure()
    {
        setTitle(getTitlePage());
        setSizePage();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);  
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                dispose();        
            }
        });
    }
    
    /**
     * Configurar la toma de datos
     */
    protected void configureFieldsData()
    {
        numPairs = 0;
        JLabel label;
        
        panel = new JPanel(new SpringLayout());
        getContentPane().add(panel);
        
        label = new JLabel("Material: ", JLabel.TRAILING);
        suscribiblePicker = new JComboBox(((SuscripcionViewModel)viewModel).getItemsSuscribiblesList());        
        
        // Modificar la forma en que se pinta el contenido del combo para que sólo se muestre el título del item
        suscribiblePicker.setRenderer(new ListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

                value = ((ISuscribible)value).getTitle();
                Component component = new DefaultListCellRenderer()
                        .getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

                return component;
            }
        });        
        
        panel.add(label);
        panel.add(suscribiblePicker);
        suscribiblePicker.setSelectedIndex(0);
        ++numPairs;    
        
        label = new JLabel("Periodicidad: ", JLabel.TRAILING);
        periodicidadPicker = new JComboBox(((SuscripcionViewModel)viewModel).getPeriodicidadList());        
        panel.add(label);
        panel.add(periodicidadPicker);
        periodicidadPicker.setSelectedIndex(0);
        ++numPairs;    
        
        putButtons();
        
        SpringUtilities.makeCompactGrid(panel,
            numPairs, 2, //rows, cols
            6, 6,        //initX, initY
            6, 6);       //xPad, yPad        
        
    }
    
    /**
     * Poner los botones de aceptar y cancelar
     */
    protected void putButtons()
    {
        // Botón para almacenar
        JButton okButton = new JButton("Ok");
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {     
                if (validateFields()) {
                    
                    // Pasar el item al viewmodel para que lo inserte
                    viewModel.insert(prepareItem());
                    
                    // Limpiar los campos
                    clearEntries();
                    
                    JOptionPane.showMessageDialog(null, "Suscripción almacenada correctamente");
                    
                }
            }
        });               
        
        // Botón para anular
        JButton cancelButton = new JButton("Cancelar");
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {     
                // Limpiar los campos
                clearEntries();
            }
        });               
        panel.add(okButton);
        panel.add(cancelButton);
        ++numPairs;        
    }
    
    /**
     * Prepara la suscripción para almacenarla
     */
    Suscripcion prepareItem()
    {
        Suscripcion item = new Suscripcion();
        item.setItem((ISuscribible)suscribiblePicker.getSelectedItem());
        item.setPeriodicidad(Periodicidad.valueOf((String)periodicidadPicker.getSelectedItem()));
        return item;
    }
    
    /**
     * Limpia la toma de datos
     */
    void clearEntries()
    {
        suscribiblePicker.setSelectedIndex(0);
        periodicidadPicker.setSelectedIndex(0);
    }
  
    /**
     * Valida la toma de datos
     * Ahora mismo no se me ocurre nada que validar aquí ya que todo se pide, por ahora, en combos.
     * Lo dejo preparado para futuros cambios en el modelo.
     */
    protected boolean validateFields()
    {
        return true;
    }    
}
