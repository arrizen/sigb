import Views.LoginPage;
import Models.Person;

/**
 * Clase principal de la aplicacion
 * 
 * @author (Rafa Leon) 
 * @version (@2016)
 */
public class Sigb
{    
    /**
     * Punto de entrada de la aplicación
     */
    public static void main (String [] args)
    {
        Sigb sigb = new Sigb();
        sigb.login();
    }    
    
    /**
     * Contructor
     */
    public Sigb()
    {
    }
    
    /**
     * Hace el login
     */
    void login()
    {
        LoginPage page = new LoginPage();
        page.setVisible(true);
    }
   
}
